# K8s
## K8s Local con Minikube
```
minikube start \
--profile=cloud-computing \
--memory=2048 \
--cpus=2 \
--disk-size=5g \
--kubernetes-version=v1.22.3 \
--driver=docker

minikube profile cloud-computing
```

## plugins minikube
- minikube addons enable ingress
- minikube addons enable metrics-server

```
kubectl get nodes

kubectl apply -f  deployment.yaml

kubectl get all

kubectl apply -f  nginx-service.yaml

kubectl get svc

minikube ip


kubectl delete pod --selector app=nginx
kubectl get pod --watch


kubectl run -i --rm --restart=Never curl-client --image=curlimages/curl --command -- curl -s 'http://nginx-service:80'
```


### Fin
- minikube delete --profile  cloud-computing


# K8s Cluster CloudComputing 

## agregar config local

- config a ./kube/config

### agregar a gitlab variable
- cat config | base64 > config.txt
- cat config.txt copiar a KUBE_CONFIG

#### CICI con k8s

- crear archivo .gitlab-ci.yml
```

stages:
  - deploy

deploy-k8s:
  stage: deploy
  image:
    name: bitnami/kubectl:latest
    entrypoint: [ "" ]
  before_script:
    - echo $KUBE_CONFIG | base64 -d > config
    - mv config /.kube/
  environment:
    name: cloudcomputing-$grupo
    url: http://k8s-lia.unrn.edu.ar/$grupo
    kubernetes:
      namespace: $grupo
  script:
    - kubectl config get-contexts
    - kubectl config use-context microk8s
    - kubectl config set-context --current --namespace=$grupo
    - kubectl get pods
    # hacer el deploy mediante kubectl 
  only:
    - to-k8s
```
